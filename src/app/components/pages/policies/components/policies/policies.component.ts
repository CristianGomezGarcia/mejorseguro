import { Component, OnInit, ViewChild } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { ApplicationSettings, Page } from '@nativescript/core';
import { PolicieModel } from '~/app/core/Models/Policie/Policie@Model';
import { PoliciesService } from '~/app/core/Services/Policies/policies.service';

@Component({
    selector: 'app-policies',
    templateUrl: 'policies.component.html',
    styleUrls: ['policies.component.scss']
})

export class PoliciesComponent implements OnInit {

    dataLoaded: boolean = false;
    textLoader: string = 'Cargando pólizas\nEspere un momento, por favor.';
    userActive: number;
    policiesArr: Array<PolicieModel> = [];

    constructor(
        private routerExtensions: RouterExtensions,
        private policiesService: PoliciesService,
        private page: Page
    ) {
        this.userActive = ApplicationSettings.getNumber('userActive');
        this.page.actionBarHidden = true;
        this.getPolicies();
    }

    ngOnInit() { }

    getPolicies() {
        this.policiesService.getPolicies(this.userActive)
            // tslint:disable-next-line: deprecation
            .subscribe((data: Array<PolicieModel>) => {
                this.policiesArr = data;
                for (let index = 0; index < this.policiesArr.length; index++) {
                    const fechaSplit = this.policiesArr[index].fechaInicio.split('-');
                    this.policiesArr[index].fechaInicio = fechaSplit[2] + ' ' + this.getMonthName(Number(fechaSplit[1])) + ' ' + fechaSplit[0];
                    this.policiesArr[index].fechaFin = fechaSplit[2] + ' ' + this.getMonthName(Number(fechaSplit[1])) + ' ' + (Number(fechaSplit[0]) + 1).toString();
                    this.policiesArr[index].datos = JSON.parse(data[index].datos);
                }
            }, error => {
                console.log(error.message);
            }, () => {
                this.dataLoaded = true;
            });
    }

    private getMonthName(numberMonth: number) {
        const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        return months[numberMonth - 1];
    }

    onViewTap(file: string, policeNumer: string) {
        // tslint:disable-next-line: object-literal-shorthand
        const paramsArr = JSON.stringify({ file: file, policieNumber: policeNumer });
        this.routerExtensions.navigate(['/policies/view', paramsArr], {
            transition: {
                name: 'fade'
            }
        });
    }

    onDetailsTap(itemPolicie: PolicieModel) {
        itemPolicie.datos = JSON.stringify(itemPolicie.datos);
        this.routerExtensions.navigate(['/policies/details', JSON.stringify(itemPolicie)], {
            transition: {
                name: 'fade'
            }
        });
        itemPolicie.datos = JSON.parse(itemPolicie.datos);
    }

    selectImage(alias: string) {
        const url: string = `~/assets/iconos-aseguradoras`;
        const images = [
            { alias: "ABA", name: "aba.png" },
            { alias: "SEGUROS AFIRME", name: "afirme.png" },
            { alias: "ANA SEGUROS", name: "ana.png" },
            { alias: "AXA", name: "axa.png" },
            { alias: "BANORTE", name: "banorte.png" },
            { alias: "GENERAL DE SEGUROS", name: "general.png" },
            { alias: "GNP", name: "gnp.png" },
            { alias: "HDI", name: "hdi.png" },
            { alias: "INBURSA", name: "inbursa.png" },
            { alias: "MAPFRE", name: "mapfre.png" },
            { alias: "MIGO", name: "migo.jpg" },
            { alias: "EL POTOSI", name: "elpotosi.png" },
            { alias: "QUALITAS", name: "qualitas.png" },
            { alias: "ZURA", name: "sura.png" },
            { alias: "ZURICH", name: "zurich.png" },
            { alias: "EL AGUILA", name: "elaguila.png" },
            { alias: "AIG", name: "aig.png" },
            { alias: "LA LATINO", name: "lalatino.png" }
        ];

        return `${url}/${images.find((e) => e.alias === alias).name}`;
    }

}